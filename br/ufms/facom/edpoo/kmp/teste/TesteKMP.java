package br.ufms.facom.edpoo.kmp.teste;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import br.ufms.facom.edpoo.kmp.KMP;

public class TesteKMP {

	public static void main(String[] args) throws IOException {
		
		String padrao = args[0]; //argumento 1 = PAT
		int linha = 1;
		int localizado = -1;
		int[] posicao = new int[ padrao.length() ];
		BufferedReader br = new BufferedReader(new FileReader(args[1])); //argumento 2 = arquivo

		if( args[0] == null || args[1] == null)
			System.exit(1);
		
		posicao = KMP.calculaPrefixo(padrao, posicao);
		
		while(br.ready()) {			
			
			localizado =  KMP.procurar(br.readLine(), padrao, posicao);
			
			if( localizado > -1 )
				break;
			
			linha++;
		}

		br.close();
		
		if( localizado == -1 )
			System.out.println("Padr�o n�o localizado.");
		else
			System.out.println("Padr�o '" + padrao + "' localizado na linha " + linha );
	}
}
