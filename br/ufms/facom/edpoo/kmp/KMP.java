package br.ufms.facom.edpoo.kmp;

/**
 * Implementa��o do algortimo KMP idealizado para o problema de busca de substring em um texto.
 * @author Marco Ot�vio Duarte de Almeida (otavio.marco91@gmail.com)
 * @author Iohan Zukeram (iohanzuke@gmail.com)
 *
 */

public class KMP {
	
	/**
	 * M�todo para indexa��o do padr�o/palavra a ser buscada. Pr�-processamento
	 * @param padrao
	 *            Padr�o ou palavra a ser buscada
	 * @param posicao
	 *            Vetor receber� as posi��es indexadas
	 * @return
	 *            Vetor indexado com o padr�o/palavra
	 */
	
	public static int[] calculaPrefixo(String padrao, int[] posicao) {
		
		int i = 1;
		int j = 0;
		char[] arrayPadrao = padrao.toCharArray();
		
		while( i < arrayPadrao.length ) {
					
			if( arrayPadrao[i] == arrayPadrao[j] ) {
				j++;				
				posicao[i] = j;
				i++;
			}
			
			else {
				if( j != 0 )
					j = posicao[ j - 1 ];
				
				else {
					posicao[i] = 0;
					++i;
				}					
			}
		}
		
		return posicao;
	}
	
	/**
	 * M�todo de busca de uma palavra/padr�o em um texto
	 * @param texto
	 *            Texto completo, onde ser� buscado.
	 * @param padrao
	 *            Padr�o/Palavra a ser buscado.
	 * @param posicao
	 *            Vetor indexado com a palavra/padr�o.
	 * @return
	 *            Retorna a posi��o onde foi localizado padr�o ou -1 quando n�o localizar.
	 */
	
	
	public static int procurar(String texto, String padrao, int[] posicao ) {
		
		/**
		 * indexTexto
		 *            Index para andar pelo texto. 
		 * indexPadrao
		 *            Index para andar pelo padr�o/palavra.
		 * arrayTexto
		 *            Array de char baseado no texto (para f�cil itera��o).
		 * arrayPadrao
		 *            Array de char baseado no padr�o/palavra (para f�cil itera��o).
		 */
		
		int indexTexto = 0;
		int indexPadrao = 0;
		char[] arrayTexto = texto.toCharArray();
		char[] arrayPadrao = padrao.toCharArray();
		
		/**
		 * Percorre o texto inteiro at� encontrar o padr�o/palavra ou acabar o texto. 
		 */
		
		while( indexTexto < arrayTexto.length ) {
			
			if( arrayPadrao[indexPadrao] == arrayTexto[indexTexto] ) {
				indexPadrao++;
				indexTexto++;
			}
			
			if( indexPadrao == arrayPadrao.length ) {;
				indexPadrao = posicao[ indexPadrao - 1 ];
				return (indexTexto - indexPadrao); //esse return n�o faz parte da implementa��o padr�o do algoritmo. Deste modo deve ser removidda quando enviado para um trabalho completo.
			}
			
			if( indexTexto < arrayTexto.length && arrayPadrao[indexPadrao] != arrayTexto[indexTexto] ) {
				
				if( indexPadrao > 0 )
					indexPadrao = posicao[ indexPadrao - 1 ];
				
				else
					indexTexto++;
			}
			
		}
		
		return -1;
	}
}